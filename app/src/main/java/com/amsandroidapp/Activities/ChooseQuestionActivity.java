package com.amsandroidapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.ListView;

import com.amsandroidapp.adapters.QuestionListAdapter;
import com.amsandroidapp.bo.chooseQuestionActivity.QuestionListItem;
import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.common.ObjectFromXml;
import com.amsandroidapp.bo.mockExamActivity.Question;
import com.amsandroidapp.bo.mockExamActivity.Section;
import com.amsandroidapp.bo.questionReview.QuestionReviewMessageConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cilla on 28/3/2016.
 */
public class ChooseQuestionActivity extends Activity implements OnClickListener{
    private List<QuestionListItem> questionListItem = new ArrayList<>();
    private String sectionName, topicName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_topic);

        Bundle bundle = getIntent().getExtras();
        sectionName = bundle.getString(QuestionReviewMessageConstants.BUNDLE_PASSING_SECTION_NAME);
        topicName = bundle.getString(QuestionReviewMessageConstants.BUNDLE_PASSING_TOPIC_NAME);
        Button btnMockExam = (Button)findViewById(R.id.btn_mock_exam_choose_topic);
        Button btnBack = (Button)findViewById(R.id.btn_back_fragment_choose_topic);
        btnBack.setOnClickListener(this);
        btnMockExam.setOnClickListener(this);

        new renderListView().execute(sectionName, topicName);
    }

    private void CreateListView(){
        ArrayAdapter<QuestionListItem> adapter = new QuestionListAdapter(ChooseQuestionActivity.this,
                R.layout.question_list_item, questionListItem, sectionName, topicName);
        ListView chooseQuestionListView = (ListView)this.findViewById(R.id.listview);
        chooseQuestionListView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, ChooseTopicActivity.class);
        startActivity(intent);
        ChooseQuestionActivity.this.finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_back_fragment_choose_topic:
                intent = new Intent(this, ChooseTopicActivity.class);
                startActivity(intent);
                ChooseQuestionActivity.this.finish();
                break;
            case R.id.btn_mock_exam_choose_topic:
                intent = new Intent(this, MockExamActivity.class);
                startActivity(intent);
                ChooseQuestionActivity.this.finish();
                break;
            default:
                Log.e(ActivityTagConstants.CHOOSE_QUESTION_ACTIVITY_TAG, "intent not found");
                break;
        }
    }

    private class renderListView extends AsyncTask<String, Void, Void> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ChooseQuestionActivity.this);
            pd.setMessage(getResources().getString(R.string.pd_getting_questions));
            pd.show();
        }

        @Override
        protected Void doInBackground(String... section) {
            try {

                for(Section sectionItem : ObjectFromXml.QUESTION_PARSED_FROM_XML){
                    if(section[0].equalsIgnoreCase(sectionItem.getSection())){
                        if(section[1].equalsIgnoreCase(sectionItem.getTopic())){
                            for(Question questItem : sectionItem.getQuestions()){
                                questionListItem.add(new QuestionListItem(questItem.getId() - 1));
                            }
                        }
                    }
                }

            } catch (Exception e) {
                Log.e(ActivityTagConstants.CHOOSE_QUESTION_ACTIVITY_TAG, e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try{
                if(pd != null){
                    if(pd.isShowing())pd.dismiss();
                }
                CreateListView();
            }catch(Exception e){
                Log.e(ActivityTagConstants.CHOOSE_QUESTION_ACTIVITY_TAG, e.getMessage());
            }
        }

    }
}
