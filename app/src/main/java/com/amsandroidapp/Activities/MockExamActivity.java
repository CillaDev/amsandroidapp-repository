package com.amsandroidapp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.common.ObjectFromXml;
import com.amsandroidapp.bo.mockExamActivity.Answer;
import com.amsandroidapp.bo.mockExamActivity.ExamAnswer;
import com.amsandroidapp.bo.mockExamActivity.Options;
import com.amsandroidapp.bo.mockExamActivity.Question;
import com.amsandroidapp.bo.mockExamActivity.Section;
import com.amsandroidapp.bo.mockExamActivity.MockExamActivityConstants;
import com.amsandroidapp.services.ImageUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Cilla on 28/3/2016.
 */
public class MockExamActivity extends Activity implements OnClickListener {

    private boolean getNewestQuestion = false;
    private Button btnQuit, btnNext, btnPrevious;
    private RadioButton[] radioButton;
    private RadioGroup radioGroup;
    private TextView tvQuestionNo, tvQuestionText, tvExamTimer;
    private ImageView ivQuestionImg;

    private ArrayList<Question> questionList = new ArrayList<>();
    private ArrayList<ExamAnswer> selectedAnsList = new ArrayList<>();
    private List<Section> sectionList;
    private CountDownTimer examCountDownTimer;

    private int currentPosition = 0;
    private int lastPosition = 0;
    private int correctAnswerInSectA = 0;
    private int correctAnswerInSectB = 0;
    private int correctAnswerInSectC = 0;
    private int correctAnswerInSectD = 0;
    private int correctAnswerInSectE = 0;
    private int correctAnswerInSectF = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_exam);

        btnNext = (Button) findViewById(R.id.btn_next_mock_exam);
        btnPrevious = (Button) findViewById(R.id.btn_previous_mock_exam);
        btnQuit = (Button) findViewById(R.id.btn_quit_mock_exam);
        tvQuestionNo = (TextView) findViewById(R.id.tv_mock_exam_question_no);
        tvQuestionText = (TextView) findViewById(R.id.tv_mock_exam_quesion_text);
        ivQuestionImg = (ImageView) findViewById(R.id.iv_mock_exam_question_image);
        tvExamTimer = (TextView) findViewById(R.id.tv_mock_exam_timer);
        radioGroup = (RadioGroup) findViewById(R.id.rg_mock_exam_ans);


        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        btnQuit.setOnClickListener(this);

        btnPrevious.setVisibility(View.INVISIBLE);

        examCountDownTimer = new CountDownTimer(MockExamActivityConstants.DEFAULT_EXAM_DURATION * 1000, MockExamActivityConstants.TIMER_UPDATE_INTERVAL){ // DEFAULT 60 SEC
            @Override
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                String minSec = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                tvExamTimer.setText(minSec);
            }

            @Override
            public void onFinish() {
                showExamEndDialogue();
            }
        };
        new getExamQuestion().execute();
    }

    private void updateView(int index) {
        if (currentPosition >= MockExamActivityConstants.TOTAL_QUESTION_NUMBER - 1) {
            btnNext.setText(getResources().getText(R.string.btn_finish));
        }

        Question question = questionList.get(index);

        String questionText = question.getQuestionText() + "\n";
        List<Options> optionList = question.getOptions();
        String imgBase64 = question.getImgBase64();

        for (int i = 0; i < optionList.size(); i++) {
            if(!optionList.get(i).getOptionText().isEmpty()) {
                questionText += optionList.get(i).getOptionText() + "\n";
            }
        }

        tvQuestionNo.setText(getResources().getText(R.string.tv_question) + Integer.toString(currentPosition + 1));
        tvQuestionText.setText(questionText);

        if(!imgBase64.isEmpty()) {
            Bitmap bitmap = ImageUtils.decodeBase64(imgBase64);
            ivQuestionImg.setVisibility(View.VISIBLE);
            ivQuestionImg.setImageBitmap(bitmap);
        }else {
            ivQuestionImg.setImageDrawable(null);
            ivQuestionImg.setVisibility(View.GONE);
        }

        List<Answer> answerList = questionList.get(index).getAnswers();

        radioButton = new RadioButton[answerList.size()];
        radioGroup.clearCheck();
        radioGroup.removeAllViews();
        for (int i = 0; i < answerList.size(); i++) {
            radioButton[i] = new RadioButton(MockExamActivity.this);
            radioButton[i].setText(answerList.get(i).getAnswerText());
            radioGroup.addView(radioButton[i]);
        }

        // display chosen selection
        if(!getNewestQuestion && selectedAnsList.size() > index){
            String currRadioBtnSelection = selectedAnsList.get(index).getRadioBtnSelection();

            if("A".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[0].setChecked(true);
            }else if("B".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[1].setChecked(true);
            }else if("C".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[2].setChecked(true);
            }else if("D".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[3].setChecked(true);
            }
        }
    }

    private List<Question> getQuestionFromSection(String section, int amountOfQuestion){
        List<Question> sectionQuests = new ArrayList<Question>();

        for(Section s : sectionList){
            if(section.equalsIgnoreCase(s.getSection())){
                for(Question q : s.getQuestions()){
                    sectionQuests.add(q);
                }
            }
        }

        Collections.shuffle(sectionQuests);
        return sectionQuests.subList(0, amountOfQuestion);
    }

    private void finishExam() {
        Bundle extraBundle = new Bundle();
        extraBundle.putSerializable("selectedAnsList", selectedAnsList);
        extraBundle.putSerializable("questionList", questionList);

        Intent intent = new Intent(MockExamActivity.this, MockResultActivity.class);
        intent.putExtra("extraBundle", extraBundle);
        intent.putExtra("correctAnswerInSectA", correctAnswerInSectA);
        intent.putExtra("correctAnswerInSectB", correctAnswerInSectB);
        intent.putExtra("correctAnswerInSectC", correctAnswerInSectC);
        intent.putExtra("correctAnswerInSectD", correctAnswerInSectD);
        intent.putExtra("correctAnswerInSectE", correctAnswerInSectE);
        intent.putExtra("correctAnswerInSectF", correctAnswerInSectF);

        startActivity(intent);
        MockExamActivity.this.finish();
    }

    private void checkSelectedAnswer(Question currentQuest, int index,
                                     List<Answer> ansList, boolean getNewQuest, int questId) {

        ExamAnswer currentExamAnswer;
        if(selectedAnsList.size() > index){
            currentExamAnswer = selectedAnsList.get(index);
        }else{
            currentExamAnswer = new ExamAnswer();
        }

        String currQuestSection = currentQuest.getSection().toUpperCase();
        String currQuestTopic = currentQuest.getTopic();

        currentExamAnswer.setSection(currQuestSection);
        currentExamAnswer.setTopic(currQuestTopic);
        currentExamAnswer.setQuestionId(questId);

        int rgId = radioGroup.getCheckedRadioButtonId();

        if (rgId >= 0) {
            int reminder = rgId % ansList.size();

            if (reminder == 0) {
                // MC choice is "D"
                if((ansList.get(ansList.size() - 1).getIsAnswer())){
                    currentExamAnswer.setIsAnswer(true);

                    sortMarksBySection(currQuestSection);

                }else{
                    currentExamAnswer.setIsAnswer(false);
                }
                String currRadioBtnSelection = ansList.get(ansList.size() - 1).getAnswerText().substring(0,1);
                currentExamAnswer.setRadioBtnSelection(currRadioBtnSelection);
            } else {
                // MC choice is "A"=1 or "B"=2 or "C"=3
                if (ansList.get(reminder - 1).getIsAnswer()){
                    currentExamAnswer.setIsAnswer(true);

                    sortMarksBySection(currQuestSection);

                }else{
                    currentExamAnswer.setIsAnswer(false);
                }
                String currRadioBtnSelection = ansList.get(reminder - 1).getAnswerText().substring(0,1);
                currentExamAnswer.setRadioBtnSelection(currRadioBtnSelection);
            }
        }else{
            // previous question
        }

        if(selectedAnsList.size() > index && getNewQuest) {
            selectedAnsList.add(currentExamAnswer);

        }else if(selectedAnsList.size() > index){
            selectedAnsList.get(index).setRadioBtnSelection(currentExamAnswer.getRadioBtnSelection());
            selectedAnsList.get(index).setIsAnswer(currentExamAnswer.getIsAnswer());
            selectedAnsList.get(index).setSection(currQuestSection);
            selectedAnsList.get(index).setTopic(currQuestTopic);

        }else{
            selectedAnsList.add(currentExamAnswer);
        }

    }

    private void sortMarksBySection(String section){
        switch(section){
            case "A":
                correctAnswerInSectA++;
                break;
            case "B":
                correctAnswerInSectB++;
                break;
            case "C":
                correctAnswerInSectC++;
                break;
            case "D":
                correctAnswerInSectD++;
                break;
            case "E":
                correctAnswerInSectE++;
                break;
            case "F":
                correctAnswerInSectF++;
                break;
        }
    }

    private void getNextQuestion(final int index) {
        runOnUiThread(new Runnable() {
            public void run() {
                if(currentPosition > 0){
                    btnPrevious.setVisibility(View.VISIBLE);
                }else{
                    btnPrevious.setVisibility(View.INVISIBLE);
                }
                updateView(index);
            }
        });
    }

    public void showExamEndDialogue() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.alert_dialog_exam_title));
        alertDialog.setMessage(getResources().getString(R.string.alert_dialog_exam_message));
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.btn_okay),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        finishExam();
                    }
                });
        try{
            alertDialog.show();
        }catch(Exception e){
            Log.e(ActivityTagConstants.MOCK_EXAM_ACTIVITY_TAG, e.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_previous_mock_exam:
                getNewestQuestion = false;
                currentPosition--;
                getNextQuestion(currentPosition);
                break;

            case R.id.btn_next_mock_exam:
                if (currentPosition >= MockExamActivityConstants.TOTAL_QUESTION_NUMBER - 1) {
                    Log.d(ActivityTagConstants.MOCK_EXAM_ACTIVITY_TAG, "-------------Last Question---------");
                    checkSelectedAnswer(questionList.get(currentPosition), currentPosition,
                            questionList.get(currentPosition).getAnswers(), getNewestQuestion,
                            questionList.get(currentPosition).getId());
                    finishExam();

                } else {
                    checkSelectedAnswer(questionList.get(currentPosition), currentPosition,
                            questionList.get(currentPosition).getAnswers(), getNewestQuestion,
                            questionList.get(currentPosition).getId());
                    if(currentPosition == lastPosition){
                        getNewestQuestion = true;
                        currentPosition++;
                        lastPosition++;
                        getNextQuestion(lastPosition);
                    }else{
                        getNewestQuestion = false;
                        currentPosition++;
                        getNextQuestion(currentPosition);
                    }
                }
                break;

            case R.id.btn_quit_mock_exam:
                examCountDownTimer.cancel();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                MockExamActivity.this.finish();
                break;
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        MockExamActivity.this.finish();
    }

    private class getExamQuestion extends AsyncTask<Void, String, Void> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MockExamActivity.this);
            pd.setMessage(getResources().getString(R.string.pd_getting_questions));
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            sectionList = ObjectFromXml.QUESTION_PARSED_FROM_XML;

            questionList.addAll(getQuestionFromSection("A", MockExamActivityConstants.SECTION_A_QUESTION_AMOUNT));
            questionList.addAll(getQuestionFromSection("B", MockExamActivityConstants.SECTION_B_QUESTION_AMOUNT));
            questionList.addAll(getQuestionFromSection("C", MockExamActivityConstants.SECTION_C_QUESTION_AMOUNT));
            questionList.addAll(getQuestionFromSection("D", MockExamActivityConstants.SECTION_D_QUESTION_AMOUNT));
            questionList.addAll(getQuestionFromSection("E", MockExamActivityConstants.SECTION_E_QUESTION_AMOUNT));
            questionList.addAll(getQuestionFromSection("F", MockExamActivityConstants.SECTION_F_QUESTION_AMOUNT));

            Collections.shuffle(questionList);

            MockExamActivityConstants.TOTAL_QUESTION_NUMBER = questionList.size();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (pd != null) {
                    if (pd.isShowing()) pd.dismiss();
                }

                updateView(0);
                examCountDownTimer.start();

            } catch (Exception e) {
                Log.e(ActivityTagConstants.MOCK_EXAM_ACTIVITY_TAG, e.toString());
            }
        }
    }
}
