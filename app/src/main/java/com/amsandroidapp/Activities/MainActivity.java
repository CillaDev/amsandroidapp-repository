package com.amsandroidapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.common.ObjectFromXml;
import com.amsandroidapp.bo.mockExamActivity.Section;
import com.amsandroidapp.services.XmlService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnMockExam = (Button)findViewById(R.id.btn_exam_question);
        Button btnRevision = (Button)findViewById(R.id.btn_revision);
        btnMockExam.setOnClickListener(this);
        btnRevision.setOnClickListener(this);
        new parseXml().execute();
    }

    @Override
    public void onClick(View v){
        Intent intent;
        switch(v.getId()){
            case R.id.btn_exam_question:
/*--------- Develop use only ------------------------------------------------*/
//                intent = new Intent(this, MockResultActivity.class);
//                intent.putExtra("correctAns", 15);
//                intent.putExtra("totalScore", 40);
//
//                ArrayList<Integer> answerArray = new ArrayList<Integer>();
//
//                for(int i = 0; i < 28; i++){
//                    answerArray.add(1);
//                }
//
//                for(int j = 0; j < 12; j ++){
//                    answerArray.add(0);
//                }
//
//                Collections.shuffle(answerArray);
//
//                intent.putIntegerArrayListExtra("answerArray", answerArray);
/*-------------------------------------------------------------------------*/
                intent = new Intent(this, MockExamActivity.class);
                startActivity(intent);
                MainActivity.this.finish();
                break;

            case R.id.btn_revision:
                intent = new Intent(this, ChooseTopicActivity.class);
                startActivity(intent);
                MainActivity.this.finish();
                break;
            default:
                Log.e(ActivityTagConstants.MAIN_ACTIVITY_TAG, "intent not found");
                break;
        }
    }

    private class parseXml extends AsyncTask<Void, String, Void>{
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage(getResources().getString(R.string.pd_getting_questions));
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try{
                XmlService xmlService = new XmlService(MainActivity.this, R.raw.sample50more);
                ObjectFromXml.QUESTION_PARSED_FROM_XML = xmlService.parseXML();

            }catch (Exception e) {
                Log.e(ActivityTagConstants.MOCK_EXAM_ACTIVITY_TAG, e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (pd != null) {
                    if (pd.isShowing()) pd.dismiss();
                }

            } catch (Exception e) {
                Log.e(ActivityTagConstants.MAIN_ACTIVITY_TAG, e.toString());
            }
        }
    }
}
