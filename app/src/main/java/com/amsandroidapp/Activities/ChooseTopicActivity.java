package com.amsandroidapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.amsandroidapp.bo.chooseTopicActivity.TopicListItem;
import com.amsandroidapp.adapters.TopicListAdapter;
import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.common.ObjectFromXml;
import com.amsandroidapp.bo.mockExamActivity.Section;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ChooseTopicActivity extends Activity implements OnClickListener{
    private List<TopicListItem> topicList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_topic);
        Button btnBack = (Button)findViewById(R.id.btn_back_fragment_choose_topic);
        Button btnMockExam = (Button)findViewById(R.id.btn_mock_exam_choose_topic);
        btnBack.setOnClickListener(this);
        btnMockExam.setOnClickListener(this);

        new renderListView().execute();
    }

    private void CreateListView(){
        ArrayAdapter<TopicListItem> adapter = new TopicListAdapter(ChooseTopicActivity.this, R.layout.question_list_item, topicList);
        ListView chooseTopicListView = (ListView)this.findViewById(R.id.listview);
        chooseTopicListView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        ChooseTopicActivity.this.finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_back_fragment_choose_topic:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                ChooseTopicActivity.this.finish();
                break;
            case R.id.btn_mock_exam_choose_topic:
                intent = new Intent(this, MockExamActivity.class);
                startActivity(intent);
                ChooseTopicActivity.this.finish();
                break;
            default:
                Log.e(ActivityTagConstants.CHOOSE_TOPIC_ACTIVITY_TAG, "intent not found");
                break;
        }
    }

    private class renderListView extends AsyncTask<Void, String, Void> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ChooseTopicActivity.this);
            pd.setMessage(getResources().getString(R.string.pd_getting_questions));
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                for(Section sectionItem : ObjectFromXml.QUESTION_PARSED_FROM_XML){
                    topicList.add(new TopicListItem(sectionItem.getTopic(), sectionItem.getSection()));
                }
            } catch (Exception e) {
                Log.e(ActivityTagConstants.CHOOSE_TOPIC_ACTIVITY_TAG, e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try{
                if(pd != null){
                    if(pd.isShowing())pd.dismiss();
                }
                CreateListView();
            }catch(Exception e){
                Log.e(ActivityTagConstants.CHOOSE_TOPIC_ACTIVITY_TAG, e.getMessage());
            }
        }

    }

}
