package com.amsandroidapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.mockExamActivity.ExamAnswer;
import com.amsandroidapp.bo.mockExamActivity.MockExamActivityConstants;
import com.amsandroidapp.bo.mockExamActivity.Question;

import java.util.ArrayList;

/**
 * Created by cillamok on 31/3/2016.
 *
 * 
 */
public class MockResultActivity extends Activity implements OnClickListener{
    private int totalScore;
    private int correctAnswerInSectA;
    private int correctAnswerInSectB;
    private int correctAnswerInSectC;
    private int correctAnswerInSectD;
    private int correctAnswerInSectE;
    private int correctAnswerInSectF;

    private TextView tvMockResultPass;
    private TextView tvMockResultScore;
    private TextView tvMockResultSectAMark;
    private TextView tvMockResultSectBMark;
    private TextView tvMockResultSectCMark;
    private TextView tvMockResultSectDMark;
    private TextView tvMockResultSectEMark;
    private TextView tvMockResultSectFMark;
    private Button btnDone;

    private ArrayList<ExamAnswer> selectedAnsList;
    private ArrayList<Question> questionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_result);

        Bundle bundle = getIntent().getExtras();
        Bundle extraBundle = getIntent().getBundleExtra("extraBundle");

        if(bundle != null){
            selectedAnsList = (ArrayList<ExamAnswer>) extraBundle.getSerializable("selectedAnsList");
            questionList = (ArrayList<Question>) extraBundle.getSerializable("questionList");

            correctAnswerInSectA = bundle.getInt("correctAnswerInSectA");
            correctAnswerInSectB = bundle.getInt("correctAnswerInSectB");
            correctAnswerInSectC = bundle.getInt("correctAnswerInSectC");
            correctAnswerInSectD = bundle.getInt("correctAnswerInSectD");
            correctAnswerInSectE = bundle.getInt("correctAnswerInSectE");
            correctAnswerInSectF = bundle.getInt("correctAnswerInSectF");
            totalScore = selectedAnsList.size();
        }else{
            Log.e(ActivityTagConstants.MOCK_RESULT_ACTIVITY_TAG, "no result.");
        }

        tvMockResultPass = (TextView)findViewById(R.id.tv_mock_result_pass);
        tvMockResultScore = (TextView)findViewById(R.id.tv_mock_result_score);
        tvMockResultSectAMark = (TextView)findViewById(R.id.tv_mock_result_section_a_score);
        tvMockResultSectBMark = (TextView)findViewById(R.id.tv_mock_result_section_b_score);
        tvMockResultSectCMark = (TextView)findViewById(R.id.tv_mock_result_section_c_score);
        tvMockResultSectDMark = (TextView)findViewById(R.id.tv_mock_result_section_d_score);
        tvMockResultSectEMark = (TextView)findViewById(R.id.tv_mock_result_section_e_score);
        tvMockResultSectFMark = (TextView)findViewById(R.id.tv_mock_result_section_f_score);

        calculateScoresAndSetTextView();

        GridView gridview = (GridView) findViewById(R.id.gridView);
        gridview.setAdapter(new MockExamResultAdapter());
        gridview.setNumColumns(4);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle extraBundle = new Bundle();
                extraBundle.putSerializable("selectedAnsList", selectedAnsList);
                extraBundle.putSerializable("questionList", questionList);

                Intent intent = new Intent(MockResultActivity.this, MockExamReviewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("extraBundle", extraBundle);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        btnDone = (Button)findViewById(R.id.btn_done_mock_result);
        btnDone.setOnClickListener(this);

    }

    private void calculateScoresAndSetTextView(){
        int correctAns = 0;

        for(int i = 0; i < selectedAnsList.size(); i++){
            if(selectedAnsList.get(i).getIsAnswer()){
                correctAns++;
            }
        }

        double score = (double)correctAns / totalScore * 100;
        tvMockResultScore.setText(String.format("%.2f", score) + getResources().getString(R.string.tv_mock_result_percentage));

        // fail if result < 70%
        if(score >= 70 ){
            tvMockResultPass.setText(getResources().getString(R.string.tv_mock_result_pass));
        }else{
            tvMockResultPass.setText(getResources().getString(R.string.tv_mock_result_fail));
        }

        double sectionAProportion = (double)MockExamActivityConstants.SECTION_A_QUESTION_AMOUNT / totalScore * 100;
        double sectionBProportion = (double)MockExamActivityConstants.SECTION_B_QUESTION_AMOUNT / totalScore * 100;
        double sectionCProportion = (double)MockExamActivityConstants.SECTION_C_QUESTION_AMOUNT / totalScore * 100;
        double sectionDProportion = (double)MockExamActivityConstants.SECTION_D_QUESTION_AMOUNT / totalScore * 100;
        double sectionEProportion = (double)MockExamActivityConstants.SECTION_E_QUESTION_AMOUNT / totalScore * 100;
        double sectionFProportion = (double)MockExamActivityConstants.SECTION_F_QUESTION_AMOUNT / totalScore * 100;

        double scoreA = (double)(MockExamActivityConstants.SECTION_A_QUESTION_AMOUNT - correctAnswerInSectA)/ MockExamActivityConstants.SECTION_A_QUESTION_AMOUNT * sectionAProportion;
        double scoreB = (double)(MockExamActivityConstants.SECTION_B_QUESTION_AMOUNT - correctAnswerInSectB)/ MockExamActivityConstants.SECTION_B_QUESTION_AMOUNT * sectionBProportion;
        double scoreC = (double)(MockExamActivityConstants.SECTION_C_QUESTION_AMOUNT - correctAnswerInSectC)/ MockExamActivityConstants.SECTION_C_QUESTION_AMOUNT * sectionCProportion;
        double scoreD = (double)(MockExamActivityConstants.SECTION_D_QUESTION_AMOUNT - correctAnswerInSectD)/ MockExamActivityConstants.SECTION_D_QUESTION_AMOUNT * sectionDProportion;
        double scoreE = (double)(MockExamActivityConstants.SECTION_E_QUESTION_AMOUNT - correctAnswerInSectE)/ MockExamActivityConstants.SECTION_E_QUESTION_AMOUNT * sectionEProportion;
        double scoreF = (double)(MockExamActivityConstants.SECTION_F_QUESTION_AMOUNT - correctAnswerInSectF)/ MockExamActivityConstants.SECTION_F_QUESTION_AMOUNT * sectionFProportion;

        tvMockResultSectAMark.setText("-" + String.format("%.2f", scoreA) + getResources().getString(R.string.tv_mock_result_percentage));
        tvMockResultSectBMark.setText("-" + String.format("%.2f", scoreB) + getResources().getString(R.string.tv_mock_result_percentage));
        tvMockResultSectCMark.setText("-" + String.format("%.2f", scoreC) + getResources().getString(R.string.tv_mock_result_percentage));
        tvMockResultSectDMark.setText("-" + String.format("%.2f", scoreD) + getResources().getString(R.string.tv_mock_result_percentage));
        tvMockResultSectEMark.setText("-" + String.format("%.2f", scoreE) + getResources().getString(R.string.tv_mock_result_percentage));
        tvMockResultSectFMark.setText("-" + String.format("%.2f", scoreF) + getResources().getString(R.string.tv_mock_result_percentage));

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_done_mock_result:
                Intent intent = new Intent(MockResultActivity.this, MainActivity.class);
                startActivity(intent);
                MockResultActivity.this.finish();
                break;
        }
    }

    public class MockExamResultAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if(null != selectedAnsList){
                return selectedAnsList.size();
            }else return 0;

        }

        @Override
        public Object getItem(int position) {
            return selectedAnsList.get(position).getIsAnswer();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View grid = convertView;

            if(grid == null){
                grid = getLayoutInflater().inflate(R.layout.mock_result_grid_item, parent, false);
            }

            TextView textView = (TextView)grid.findViewById(R.id.tv_mock_result_grid);
            textView.setText(Integer.toString(position + 1));

            if(selectedAnsList.get(position).getIsAnswer()){ // true = correct answer
                textView.setBackgroundResource(R.drawable.correct_rounded_corner);
            }else{
                textView.setBackgroundResource(R.drawable.worng_rounded_corner);
            }

            return grid;
        }
    }
}
