/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.amsandroidapp.services;

import android.util.Xml;

import com.amsandroidapp.bo.mockExamActivity.Answer;
import com.amsandroidapp.bo.mockExamActivity.Options;
import com.amsandroidapp.bo.mockExamActivity.Question;
import com.amsandroidapp.bo.mockExamActivity.Section;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cillamok on 31/3/2016.
 */
public class CustomXmlParser {
    private static final String ns = null;
    private String section, topic;

    public List<Section> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readXml(parser);
        } finally {
            in.close();
        }
    }

    private List<Section> readXml(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Section> sectionLists = new ArrayList<Section>();

        parser.require(XmlPullParser.START_TAG, ns, "QuestionHolder");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("Section")) {
                sectionLists.add(readSection(parser));
            } else {
                skip(parser);
            }
        }
        return sectionLists;
    }

    private Section readSection(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "Section");
        section = parser.getAttributeValue(null, "section");
        String topic = null;
        List<Question> question = new ArrayList<Question>();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Topic")) {
                topic = readTopic(parser);
            } else if (name.equals("Question")) {
                question.add(readQuestion(parser));
            } else {
                skip(parser);
            }
        }
        return new Section(question, topic, section);
    }

    private String readTopic(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Topic");
        topic = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Topic");
        return topic;
    }

    private Question readQuestion(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Question");

        int id = -1;
        String strId = parser.getAttributeValue(null, "id");
        if(null != strId){
            id = Integer.parseInt(strId);
        }

        String text = null;
        List<Options> option = new ArrayList<Options>();
        List<Answer> answer = new ArrayList<Answer>();
        String imgBase64 = "";

        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }
            String name = parser.getName();
            if(name.equals("QuestionText")){
                text = readText(parser);
            }else if(name.equals("OptionText")){
                option.add(readOption(parser));
            }else if(name.equals("ImgBase64")){
                imgBase64 = readImgBase64(parser);
            }else if(name.equals("Answer")){
                answer.add(readAnswer(parser));
            }else{
                skip(parser);
            }
        }

        parser.require(XmlPullParser.END_TAG, ns, "Question");
        return new Question(id, section, topic, text, option, imgBase64, answer);
    }

    private Options readOption(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "OptionText");
        String optionText = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "OptionText");
        return new Options(optionText);
    }

    private String readImgBase64(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "ImgBase64");
        String imgBase64 = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "ImgBase64");
        return new String(imgBase64);
    }

    private Answer readAnswer(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Answer");
        String text = null;
        Boolean isAnswer = false;

        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            String name = parser.getName();
            if(name.equals("AnswerText")){
                text = readText(parser);
            }else if(name.equals("IsAnswer")){
                isAnswer = readIsAnswer(parser);
            }else{
                skip(parser);
            }
        }

        parser.require(XmlPullParser.END_TAG, ns, "Answer");
        return new Answer(text, isAnswer, section, topic);
    }

    private Boolean readIsAnswer(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if(parser.next() == XmlPullParser.TEXT){
            result = parser.getText();
            parser.nextTag();
        }
        return Boolean.valueOf(result);
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
