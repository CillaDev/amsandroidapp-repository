package com.amsandroidapp.services.question_review;

import android.app.Activity;
import android.graphics.Bitmap;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.amsandroidapp.activities.R;
import com.amsandroidapp.bo.mockExamActivity.Answer;
import com.amsandroidapp.bo.mockExamActivity.Options;
import com.amsandroidapp.bo.mockExamActivity.Question;
import com.amsandroidapp.bo.mockExamActivity.Section;
import com.amsandroidapp.services.ImageUtils;

import java.util.List;

/**
 * Created by dickson on 10/4/2016.
 */
public class QuestionReviewService {

    public Question loadQuestion(int questionId, String sectionName, String topicName, List<Section> sectionList) {
        Question question = null;
        for (Section section : sectionList) {
            if (section.getSection().equalsIgnoreCase(sectionName)) {
                if(section.getTopic().equalsIgnoreCase(topicName)) {
                    question = section.getQuestions().get(questionId);
                }
            }
        }
        return question;
    }

    public String loadQuestionText(Question question) {
        String questionText = question.getQuestionText() + "\n";
        List<Options> optionList = question.getOptions();

        for (int i = 0; i < optionList.size(); i++) {
            if (!optionList.get(i).getOptionText().isEmpty()) {
                questionText += optionList.get(i).getOptionText() + "\n";
            }
        }
        return questionText;
    }

    public Bitmap loadQuestionImage(Question question) {
        String imgBase64 = question.getImgBase64();
        if(!imgBase64.isEmpty()) {
            Bitmap bitmap = ImageUtils.decodeBase64(imgBase64);
            return bitmap;
        }
        return null;
    }


    public String loadQuestionTitle(Activity activity, String sectionName, int questionId) {
        return activity.getResources().getText(R.string.tv_question) + sectionName + "-" + questionId;
    }

    public void loadRadioGroup(Activity activity, Question question, RadioGroup rgOptions, int selectedAnswer) {
        List<Answer> AnswerList = question.getAnswers();
        RadioButton[] radioButton = new RadioButton[AnswerList.size()];
        rgOptions.clearCheck();
        rgOptions.removeAllViews();
        for (int i = 0; i < AnswerList.size(); i++) {
            radioButton[i] = new RadioButton(activity);
            radioButton[i].setText(AnswerList.get(i).getAnswerText());
            radioButton[i].setClickable(false);
            if(AnswerList.get(i).getIsAnswer() == true) {
                radioButton[i].setBackgroundResource(R.drawable.correct_rounded_corner);
            }
//            if(!Integer.toString(selectedAnswer).isEmpty() && selectedAnswer == i) {
//                radioButton[i].setChecked(true);
//            }
            rgOptions.addView(radioButton[i]);
        }
    }
}
