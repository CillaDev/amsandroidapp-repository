package com.amsandroidapp.services.question_review;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.amsandroidapp.activities.R;
import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.mockExamActivity.Question;
import com.amsandroidapp.bo.mockExamActivity.Section;
import com.amsandroidapp.bo.questionReview.QuestionReviewMessageConstants;
import com.amsandroidapp.services.XmlService;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

/**
 * Created by dickson on 10/4/2016.
 */
public class QuestionReviewActivityService {
    private TextView tvQuestionTitle, tvQuestionText;
    private ImageView ivQuestionImage;
    private RadioGroup rgOptions;
    private Button btnPreviousQuesiton, btnNextQuestion;

    private QuestionReviewService questionReviewService;
    private XmlService xmlService;

    private Activity activity;

    private int questionId;
    private int selectedAnswer;
    private String sectionName;
    private String topicName;
    private List<Section> sectionList;

    public QuestionReviewActivityService(Activity activity) {
        this.activity = activity;
        questionReviewService = new QuestionReviewService();
    }

    public void onCreatePreparation() {
        loadBundle();
        parseXmlAndLoadQuestion();
    }

    private void loadBundle() {
        Bundle bundle = activity.getIntent().getExtras();

        if(bundle != null) {
            questionId = bundle.getInt(QuestionReviewMessageConstants.BUNDLE_PASSING_QUESTION_ID, -1);
            sectionName = bundle.getString(QuestionReviewMessageConstants.BUNDLE_PASSING_SECTION_NAME);
            topicName = bundle.getString(QuestionReviewMessageConstants.BUNDLE_PASSING_TOPIC_NAME);
            selectedAnswer = bundle.getInt(QuestionReviewMessageConstants.BUNDLE_PASSING_SELECTED_ANSWER);
            if (questionId == -1) {
                Log.e(ActivityTagConstants.QUESTION_REVIEW_ACTIVITY_TAG, QuestionReviewMessageConstants.ERROR_QUESTION_ID_MISSING);
                activity.finish();
                return;
            }
        }else {
            Log.e(ActivityTagConstants.QUESTION_REVIEW_ACTIVITY_TAG, QuestionReviewMessageConstants.ERROR_BUNDLE_MISSING);
            activity.finish();
            return;
        }
    }

    private void parseXmlAndLoadQuestion() {
        try {
            xmlService = new XmlService(activity, R.raw.sample50more);
            sectionList = xmlService.parseXML();
            Question question = questionReviewService.loadQuestion(questionId, sectionName, topicName, sectionList);
            if (question == null) {
                Log.e(ActivityTagConstants.QUESTION_REVIEW_ACTIVITY_TAG, QuestionReviewMessageConstants.ERROR_QUESTION_NOT_EXIST);
                activity.finish();
                return;
            }
            loadQuestionIntoView(question);
        }catch(XmlPullParserException xmlPullParseException) {
            Log.e(ActivityTagConstants.QUESTION_REVIEW_ACTIVITY_TAG, QuestionReviewMessageConstants.ERROR_XML_PARSING_EXCEPTION_WHEN_PARSING);
            activity.finish();
            return;
        }catch(IOException ioException) {
            Log.e(ActivityTagConstants.QUESTION_REVIEW_ACTIVITY_TAG, QuestionReviewMessageConstants.ERROR_IO_EXCEPTION_WHEN_PARSING);
            activity.finish();
            return;
        }
    }

    private void loadQuestionIntoView(Question question) {
        tvQuestionTitle.setText(questionReviewService.loadQuestionTitle(activity, sectionName, questionId));
        tvQuestionText.setText(questionReviewService.loadQuestionText(question));
        ivQuestionImage.setImageBitmap(questionReviewService.loadQuestionImage(question));
        questionReviewService.loadRadioGroup(activity, question, rgOptions, selectedAnswer);
    }

    public void inflateView() {
        activity.setContentView(R.layout.activity_question_review);
        tvQuestionTitle = (TextView) activity.findViewById(R.id.tv_question_review_question_title);
        tvQuestionText = (TextView) activity.findViewById(R.id.tv_question_review_question_text);
        ivQuestionImage =(ImageView) activity.findViewById(R.id.iv_question_review_question_image);
        rgOptions = (RadioGroup) activity.findViewById(R.id.rg_question_review_options);
        btnPreviousQuesiton = (Button) activity.findViewById(R.id.btn_question_review_previous_question);
        btnNextQuestion = (Button) activity.findViewById(R.id.btn_question_review_next_question);

        btnPreviousQuesiton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionId -= 1;
                Question question = null;
                try{
                    question = questionReviewService.loadQuestion(questionId, sectionName, topicName, sectionList);
                }catch(ArrayIndexOutOfBoundsException e) {
                    questionId += 1;
                    Toast.makeText(activity, R.string.question_review_already_first_question, Toast.LENGTH_SHORT).show();
                    return;
                }
                loadQuestionIntoView(question);
            }
        });

        btnNextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionId += 1;
                Question question = null;
                try{
                    question = questionReviewService.loadQuestion(questionId, sectionName, topicName, sectionList);
                }catch(IndexOutOfBoundsException e) {
                    questionId -= 1;
                    Toast.makeText(activity, R.string.question_review_already_last_question, Toast.LENGTH_SHORT).show();
                    return;
                }
                loadQuestionIntoView(question);
            }
        });
    }
}
