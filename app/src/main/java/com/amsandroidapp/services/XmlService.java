package com.amsandroidapp.services;

import android.app.Activity;
import android.support.annotation.RawRes;

import com.amsandroidapp.bo.mockExamActivity.Section;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by dickson on 6/4/2016.
 */
public class XmlService {
    private InputStream inputStream = null;
    private Activity activity;
    private @RawRes int xmlId;

    public XmlService(Activity activity, @RawRes int xmlId) {
        this.activity = activity;
        this.xmlId = xmlId;
    }

    public List<Section> parseXML() throws IOException, XmlPullParserException {
        try {
            inputStream = activity.getResources().openRawResource(xmlId);
            CustomXmlParser xmlParser = new CustomXmlParser();
            return xmlParser.parse(inputStream);

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
}
