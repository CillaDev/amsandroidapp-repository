package com.amsandroidapp.services;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.amsandroidapp.bo.common.ImageConstants;

/**
 * Created by dickson on 2/4/2016.
 */
public class ImageUtils {

    /**
     *
     * @param input the base64 for parsing
     * @return
     */
    public static Bitmap decodeBase64(String input)
    {
        return decodeBase64(input, ImageConstants.DEFAULT_REQUIRED_IMAGE_WIDTH, ImageConstants.DEFAULT_REQUIRED_IMAGE_HEIGHT);
    }
    /**
     *
     * @param input the base64 for parsing
     * @param reqWidth The new width we want to scale to
     * @param reqHeight The new height we want to scale to
     * @return
     */
    public static Bitmap decodeBase64(String input, int reqWidth, int reqHeight)
    {
        byte[] decodedBytes = Base64.decode(input, Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length, options);

        // Find the correct scale value. It should be the power of 2.
        int scale = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode with inSampleSize
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length, options);
        bitmap = bitmap.createScaledBitmap(bitmap, options.outWidth, options.outHeight, false);
        return bitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
