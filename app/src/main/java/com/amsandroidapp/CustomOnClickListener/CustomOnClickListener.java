package com.amsandroidapp.customOnClickListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.amsandroidapp.activities.ChooseQuestionActivity;
import com.amsandroidapp.activities.QuestionReviewActivity;
import com.amsandroidapp.bo.customOnClickListener.CustomOnClickListenerConstants;
import com.amsandroidapp.bo.questionReview.QuestionReviewMessageConstants;

/**
 * Created by Cilla on 28/3/2016.
 */
public class CustomOnClickListener implements OnClickListener{
    private final String TAG = "CustomOnClickListener";
    private int questionId;
    private String sectionName;
    private String topicName;
    private Context context;
    private String activityClass;

    public CustomOnClickListener(int questionId, String sectionName,
                                 String topicName, Context context, int activitySelector){
        this.sectionName = sectionName;
        this.topicName = topicName;
        this.context = context;
        this.questionId = questionId;
        setActivitySelector(activitySelector);
    }

    private void setActivitySelector(int activitySelector) {
        switch(activitySelector){
            case CustomOnClickListenerConstants.CHOOSE_QUESTION_ACTIVITY:
                this.activityClass = "ChooseQuestionActivity";
                break;
            case CustomOnClickListenerConstants.DETAIL_QUESTION_ACTIVITY:
                this.activityClass = "DetailQuestionActivity";
                break;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (activityClass){
            case "ChooseQuestionActivity":
                intent = new Intent(this.context, ChooseQuestionActivity.class);
//        intent.putExtra("QuestionId", this.index);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(QuestionReviewMessageConstants.BUNDLE_PASSING_SECTION_NAME, sectionName);
                intent.putExtra(QuestionReviewMessageConstants.BUNDLE_PASSING_TOPIC_NAME, topicName);
                this.context.startActivity(intent);
                ((Activity)this.context).finish();
                break;
            case "DetailQuestionActivity":
                Log.d(TAG, "Open Question Review");
                intent = new Intent(this.context, QuestionReviewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(QuestionReviewMessageConstants.BUNDLE_PASSING_SECTION_NAME, sectionName);
                intent.putExtra(QuestionReviewMessageConstants.BUNDLE_PASSING_QUESTION_ID, questionId);
                intent.putExtra(QuestionReviewMessageConstants.BUNDLE_PASSING_TOPIC_NAME, topicName);
                this.context.startActivity(intent);
                break;
            default:
                Log.e(TAG, "OnClickListener index setting error");
                break;
        }


    }
}
