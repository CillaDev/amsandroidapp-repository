package com.amsandroidapp.bo.mockExamActivity;

import java.io.Serializable;

public class Options implements Serializable {
    private String optionText;

    public Options(String optionText) {
        this.optionText = optionText;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }
}
