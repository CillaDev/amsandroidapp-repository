package com.amsandroidapp.bo.mockExamActivity;

import java.util.List;

public class QuestionHolder{
    public QuestionHolder(List<Section> sections) {
        this.sections = sections;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    private List<Section> sections;

}
