package com.amsandroidapp.bo.common;

/**
 * Created by dickson on 3/4/2016.
 */
public class ImageConstants {
    public static final int DEFAULT_REQUIRED_IMAGE_WIDTH = 1000;
    public static final int DEFAULT_REQUIRED_IMAGE_HEIGHT = 1000;
}
