package com.amsandroidapp.activities;

import android.os.Bundle;
import android.app.Activity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.amsandroidapp.services.question_review.QuestionReviewActivityService;

public class QuestionReviewActivity extends Activity {
    private QuestionReviewActivityService activityService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityService = new QuestionReviewActivityService(this);

        activityService.inflateView();
        activityService.onCreatePreparation();
    }

}
