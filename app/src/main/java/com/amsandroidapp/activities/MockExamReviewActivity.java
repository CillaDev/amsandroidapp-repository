package com.amsandroidapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.amsandroidapp.bo.common.ActivityTagConstants;
import com.amsandroidapp.bo.mockExamActivity.Answer;
import com.amsandroidapp.bo.mockExamActivity.ExamAnswer;
import com.amsandroidapp.bo.mockExamActivity.Options;
import com.amsandroidapp.bo.mockExamActivity.Question;
import com.amsandroidapp.services.ImageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cillamok on 28/Apr/2016.
 */
public class MockExamReviewActivity extends Activity implements View.OnClickListener {
    private Button btnQuit, btnNext, btnPrevious;
    private RadioButton[] radioButton;
    private RadioGroup radioGroup;
    private TextView tvQuestionNo, tvQuestionText, tvExamTimer;
    private ImageView ivQuestionImg;

    private ArrayList<Question> questionList;
    private ArrayList<ExamAnswer> selectedAnsList;
    private int currentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_exam);

        btnNext = (Button) findViewById(R.id.btn_next_mock_exam);
        btnPrevious = (Button) findViewById(R.id.btn_previous_mock_exam);
        btnQuit = (Button) findViewById(R.id.btn_quit_mock_exam);
        tvQuestionNo = (TextView) findViewById(R.id.tv_mock_exam_question_no);
        tvQuestionText = (TextView) findViewById(R.id.tv_mock_exam_quesion_text);
        ivQuestionImg = (ImageView) findViewById(R.id.iv_mock_exam_question_image);

        tvExamTimer = (TextView) findViewById(R.id.tv_mock_exam_timer);
        tvExamTimer.setVisibility(View.GONE);

        radioGroup = (RadioGroup) findViewById(R.id.rg_mock_exam_ans);

        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        btnQuit.setVisibility(View.GONE);

        getBundleInfo();

        new getExamQuestion().execute();
    }

    private void getBundleInfo(){
        Bundle extraBundle = getIntent().getBundleExtra("extraBundle");
        Bundle bundle = getIntent().getExtras();
        currentPosition = bundle.getInt("position");

        if(currentPosition > 0){
            btnPrevious.setVisibility(View.VISIBLE);
        }else{
            btnPrevious.setVisibility(View.INVISIBLE);
        }

        if(extraBundle != null){
            selectedAnsList = (ArrayList<ExamAnswer>) extraBundle.getSerializable("selectedAnsList");
            questionList = (ArrayList<Question>) extraBundle.getSerializable("questionList");
        }else{
            Log.e(ActivityTagConstants.MOCK_EXAM_REVIEW_ACTIVITY_TAG, "no result.");
        }
    }

    private void updateView(int index) {
        if(currentPosition >= selectedAnsList.size() - 1){
            btnNext.setVisibility(View.INVISIBLE);
        }else{
            btnNext.setVisibility(View.VISIBLE);
        }

        Question question = questionList.get(index);

        String questionText = question.getQuestionText() + "\n";
        List<Options> optionList = question.getOptions();
        String imgBase64 = question.getImgBase64();

        for (int i = 0; i < optionList.size(); i++) {
            if(!optionList.get(i).getOptionText().isEmpty()) {
                questionText += optionList.get(i).getOptionText() + "\n";
            }
        }

        tvQuestionNo.setText(getResources().getText(R.string.tv_question) + Integer.toString(currentPosition + 1));
        tvQuestionText.setText(questionText);

        if(!imgBase64.isEmpty()) {
            Bitmap bitmap = ImageUtils.decodeBase64(imgBase64);
            ivQuestionImg.setVisibility(View.VISIBLE);
            ivQuestionImg.setImageBitmap(bitmap);
        }else {
            ivQuestionImg.setImageDrawable(null);
            ivQuestionImg.setVisibility(View.GONE);
        }

        List<Answer> answerList = questionList.get(index).getAnswers();

        radioButton = new RadioButton[answerList.size()];
        radioGroup.clearCheck();
        radioGroup.removeAllViews();
        for (int i = 0; i < answerList.size(); i++) {
            radioButton[i] = new RadioButton(MockExamReviewActivity.this);
            radioButton[i].setText(answerList.get(i).getAnswerText());
            radioButton[i].setClickable(false);
            if(answerList.get(i).getIsAnswer()) {
                radioButton[i].setBackgroundResource(R.drawable.correct_rounded_corner);
            }
            radioGroup.addView(radioButton[i]);
        }

        // display chosen selection
        if(selectedAnsList.size() > index){
            String currRadioBtnSelection = selectedAnsList.get(index).getRadioBtnSelection();

            if("A".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[0].setChecked(true);
            }else if("B".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[1].setChecked(true);
            }else if("C".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[2].setChecked(true);
            }else if("D".equalsIgnoreCase(currRadioBtnSelection)){
                radioButton[3].setChecked(true);
            }
        }

    }

    private void getNextQuestion(final int index) {
        runOnUiThread(new Runnable() {
            public void run() {
                if(currentPosition > 0){
                    btnPrevious.setVisibility(View.VISIBLE);
                }else{
                    btnPrevious.setVisibility(View.INVISIBLE);
                }
                updateView(index);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_previous_mock_exam:
                currentPosition--;
                getNextQuestion(currentPosition);
                System.out.println("currentPosition--: " + currentPosition);
                break;

            case R.id.btn_next_mock_exam:
                currentPosition++;
                getNextQuestion(currentPosition);
                System.out.println("currentPosition++: " + currentPosition);
                break;
        }
    }

    @Override
    public void onBackPressed(){
        MockExamReviewActivity.this.finish();
    }

    private class getExamQuestion extends AsyncTask<Void, String, Void> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MockExamReviewActivity.this);
            pd.setMessage(getResources().getString(R.string.pd_getting_questions));
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (pd != null) {
                    if (pd.isShowing()) pd.dismiss();
                }

                updateView(currentPosition);

            } catch (Exception e) {
                Log.e(ActivityTagConstants.MOCK_EXAM_REVIEW_ACTIVITY_TAG, e.toString());
            }
        }
    }
}