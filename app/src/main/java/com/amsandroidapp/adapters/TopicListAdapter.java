package com.amsandroidapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.amsandroidapp.activities.R;
import com.amsandroidapp.bo.customOnClickListener.CustomOnClickListenerConstants;
import com.amsandroidapp.customOnClickListener.CustomOnClickListener;
import com.amsandroidapp.bo.chooseTopicActivity.TopicListItem;

import java.util.List;

/**
 * Created by Cilla on 27/3/2016.
 */
public class TopicListAdapter extends ArrayAdapter<TopicListItem>{
    private Activity activity;
    private List<TopicListItem> topicList;
    private CustomOnClickListener topicListOnClickListener;

    public TopicListAdapter(Context context, int resource, List<TopicListItem> topicList) {
        super(context, resource, topicList);
        this.activity = (Activity)context;
        this.topicList = topicList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = activity.getLayoutInflater().inflate(R.layout.question_list_item, parent, false);
        }

        // Find the item to work with.
        TopicListItem currentTopic = topicList.get(position);

        TextView tvTopicName = (TextView) itemView.findViewById(R.id.tv_list_item);
        tvTopicName.setText(currentTopic.getTopicName());

        topicListOnClickListener = new CustomOnClickListener(-1, currentTopic.getSectionName(), currentTopic.getTopicName(),
                this.activity, CustomOnClickListenerConstants.CHOOSE_QUESTION_ACTIVITY);

        Button btnViewQuestion = (Button) itemView.findViewById(R.id.btn_list_item);
        btnViewQuestion.setOnClickListener(topicListOnClickListener);

        return itemView;
    }
}
