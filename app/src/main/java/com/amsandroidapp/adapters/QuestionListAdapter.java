package com.amsandroidapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.amsandroidapp.activities.R;
import com.amsandroidapp.bo.chooseQuestionActivity.QuestionListItem;
import com.amsandroidapp.bo.customOnClickListener.CustomOnClickListenerConstants;
import com.amsandroidapp.customOnClickListener.CustomOnClickListener;

import java.util.List;

/**
 * Created by cillamok on 7/4/2016.
 */
public class QuestionListAdapter extends ArrayAdapter<QuestionListItem> {
    private Activity activity;
    private String sectionName, topicName;
    private List<QuestionListItem> questionList;
    private CustomOnClickListener questionListOnClickListener;

    public QuestionListAdapter(Context context, int resource, List<QuestionListItem> questionList,
                               String sectionName, String topicName){
        super(context, resource, questionList);
        this.activity = (Activity)context;
        this.questionList = questionList;
        this.sectionName = sectionName;
        this.topicName = topicName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = convertView;
        if (itemView == null) {
            itemView = activity.getLayoutInflater().inflate(R.layout.question_list_item, parent, false);
        }

        QuestionListItem currentQuestion = questionList.get(position);
        int questionId = currentQuestion.getId();
        TextView tvQuestionName = (TextView) itemView.findViewById(R.id.tv_list_item);
        tvQuestionName.setText(activity.getResources().getString(R.string.tv_question) + questionId);

        questionListOnClickListener = new CustomOnClickListener(questionId, sectionName, topicName, this.activity, CustomOnClickListenerConstants.DETAIL_QUESTION_ACTIVITY);
        Button btnViewDetailQuestion = (Button) itemView.findViewById(R.id.btn_list_item);
        btnViewDetailQuestion.setOnClickListener(questionListOnClickListener);

        return itemView;
    }
}
