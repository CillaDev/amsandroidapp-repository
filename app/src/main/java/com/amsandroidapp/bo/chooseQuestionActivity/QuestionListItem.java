package com.amsandroidapp.bo.chooseQuestionActivity;

/**
 * Created by cillamok on 7/4/2016.
 */
public class QuestionListItem {
    private int id;

    public QuestionListItem(int itemId){ this.id = itemId; }

    public int getId() {
        return id;
    }
}
