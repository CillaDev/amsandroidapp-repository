package com.amsandroidapp.bo.common;

/**
 * Created by dickson on 10/4/2016.
 */
public class ActivityTagConstants {
    public static final String QUESTION_REVIEW_ACTIVITY_TAG = "QuestionReviewActivity";
    public static final String CHOOSE_QUESTION_ACTIVITY_TAG = "ChooseQuestionActivity";
    public static final String CHOOSE_TOPIC_ACTIVITY_TAG = "ChooseTopicActivity";
    public static final String MAIN_ACTIVITY_TAG = "MainActivity";
    public static final String MOCK_EXAM_ACTIVITY_TAG = "MockExamActivity";
    public static final String MOCK_RESULT_ACTIVITY_TAG = "MockResultActivity";
    public static final String MOCK_EXAM_REVIEW_ACTIVITY_TAG = "MockExamReviewActivity";
}
