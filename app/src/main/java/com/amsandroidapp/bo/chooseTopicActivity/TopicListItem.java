package com.amsandroidapp.bo.chooseTopicActivity;

/**
 * Created by Cilla on 27/3/2016.
 */
public class TopicListItem {
    private String topicName;
    private String sectionName;

    public TopicListItem(String itemName, String sectionName) {
        this.sectionName = sectionName;
        this.topicName = itemName;
    }

    public String getTopicName(){ return this.topicName; }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
