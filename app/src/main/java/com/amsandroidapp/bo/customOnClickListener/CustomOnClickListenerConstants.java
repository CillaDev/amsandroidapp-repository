package com.amsandroidapp.bo.customOnClickListener;

/**
 * Created by cillamok on 13/Apr/2016.
 */
public class CustomOnClickListenerConstants {
    public static final int CHOOSE_QUESTION_ACTIVITY = 0;
    public static final int DETAIL_QUESTION_ACTIVITY = 1;
}
