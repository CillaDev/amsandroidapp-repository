package com.amsandroidapp.bo.questionReview;

/**
 * Created by dickson on 10/4/2016.
 */
public class QuestionReviewMessageConstants {
    public static final String BUNDLE_PASSING_QUESTION_ID = "questionId";
    public static final String BUNDLE_PASSING_SECTION_NAME = "sectionName";
    public static final String BUNDLE_PASSING_TOPIC_NAME = "topicName";
    public static final String BUNDLE_PASSING_SELECTED_ANSWER = "selectedAnswer";
    public static final String BUNDLE_PASSING_IS_SHOW_BUTTON = "isShowBtn";

    public static final String ERROR_BUNDLE_MISSING = "Bundle passing to QuestionReviewActivity is missing";
    public static final String ERROR_QUESTION_ID_MISSING = "Question id passing to QuestionReviewActivity is missing";
    public static final String ERROR_XML_PARSING_EXCEPTION_WHEN_PARSING = "XmlPullParseException occur when parsing xml";
    public static final String ERROR_IO_EXCEPTION_WHEN_PARSING = "IOException occur when parsing xml";
    public static final String ERROR_QUESTION_NOT_EXIST = "Can not found question id in section";
}

