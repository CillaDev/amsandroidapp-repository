package com.amsandroidapp.bo.mockExamActivity;

import java.io.Serializable;

public class Answer implements Serializable {
    private String answerText;
    private Boolean isAnswer = false;
    private String section;
    private String topic;

    public Answer(String answerText, Boolean isAnswer, String section, String topic) {
        this.answerText = answerText;
        this.isAnswer = isAnswer;
        this.section = section;
        this.topic = topic;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Boolean getIsAnswer() { return isAnswer; }

    public void setIsAnswer(Boolean isAnswer) {
        this.isAnswer = isAnswer;
    }

    public String getSection(){ return this.section; }

    public void setSection(String section){ this.section = section; }

    public String getTopic(){ return this.topic; }

    public void setTopic(String topic){ this.topic = topic; }
}
