package com.amsandroidapp.bo.mockExamActivity;

import java.io.Serializable;

/**
 * Created by cillamok on 28/Apr/2016.
 */
public class ExamAnswer implements Serializable {
    private Boolean isAnswer = false;
    private String section;
    private String topic;
    private String radioBtnSelection;
    private int questionId;

    public ExamAnswer() { }

    public int getQuestionId() { return questionId; }

    public void setQuestionId(int questionId) { this.questionId = questionId; }

    public void setIsAnswer(Boolean answer) {
        isAnswer = answer;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setRadioBtnSelection(String radioBtnSelection) { this.radioBtnSelection = radioBtnSelection; }

    public Boolean getIsAnswer() {
        return isAnswer;
    }

    public String getSection() {
        return section;
    }

    public String getTopic() {
        return topic;
    }

    public String getRadioBtnSelection() {
        return radioBtnSelection;
    }


}
