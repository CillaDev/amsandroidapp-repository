package com.amsandroidapp.bo.mockExamActivity;

import android.support.annotation.Nullable;

import java.util.Random;

/**
 * Created by cillamok on 13/Apr/2016.
 */
public class MockExamActivityConstants {
    public static int TOTAL_QUESTION_NUMBER;
    public static final int DEFAULT_EXAM_DURATION = 60 * 45; // 45 min
    public static final int TIMER_UPDATE_INTERVAL = 1000; // in ms

    public static int SECTION_A_QUESTION_AMOUNT = randInt(4, 5);
    public static int SECTION_B_QUESTION_AMOUNT = randInt(4, 5);
    public static int SECTION_C_QUESTION_AMOUNT = randInt(4, 5);
    public static int SECTION_D_QUESTION_AMOUNT = 9;
    public static int SECTION_E_QUESTION_AMOUNT = 12;
    public static int SECTION_F_QUESTION_AMOUNT = randInt(4, 5);


    public static int randInt(int min, int max) {

        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;;

        return randomNum;
    }
}
