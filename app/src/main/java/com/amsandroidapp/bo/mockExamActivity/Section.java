package com.amsandroidapp.bo.mockExamActivity;

import java.io.Serializable;
import java.util.List;

public class Section implements Serializable {
    private List<Question> questions;
    private String topic;
    private String section;

    public Section(List<Question> questions, String topic, String section) {
        this.questions = questions;
        this.topic = topic;
        this.section = section;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

}

