package com.amsandroidapp.bo.mockExamActivity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cillamok on 31/3/2016.
 */
public class Question implements Serializable{
    private int id;
    private String section;
    private String topic;
    private String questionText;
    private String imgBase64;
    private List<Options> options;
    private List<Answer> answers;

    public Question(int id, String section, String topic, String questionText, List<Options> options, String imgBase64, List<Answer> answers) {
        this.id = id;
        this.section = section;
        this.topic = topic;
        this.questionText = questionText;
        this.options = options;
        this.imgBase64 = imgBase64;
        this.answers = answers;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getSection() { return section; }

    public String getTopic() { return topic; }

    public void setSection(String section) { this.section = section; }

    public void setTopic(String topic) { this.topic = topic; }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public List<Options> getOptions() {
        return options;
    }

    public void setOptions(List<Options> options) {
        this.options = options;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }


}

