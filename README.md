## Best (and safest) way to merge a git branch into master ##
```
#!git
git checkout master
git pull origin master
git merge test
git push origin master
```
source:[stackoverflow](Link URL)